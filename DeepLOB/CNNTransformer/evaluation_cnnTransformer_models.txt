Prediction horizon = 0
accuracy_score = 0.8158407891718285
classification_report =               precision    recall  f1-score   support

           0     0.7004    0.5257    0.6006     21127
           1     0.8496    0.9325    0.8891     98602
           2     0.6976    0.5441    0.6113     19759

    accuracy                         0.8158    139488
   macro avg     0.7492    0.6674    0.7003    139488
weighted avg     0.8055    0.8158    0.8061    139488

-------------------------------
Prediction horizon = 1
accuracy_score = 0.7328730786877724
classification_report =               precision    recall  f1-score   support

           0     0.6147    0.4843    0.5418     27425
           1     0.7742    0.8918    0.8288     86588
           2     0.6465    0.4603    0.5378     25475

    accuracy                         0.7329    139488
   macro avg     0.6785    0.6121    0.6361    139488
weighted avg     0.7195    0.7329    0.7192    139488

-------------------------------
Prediction horizon = 2
accuracy_score = 0.7495985317733425
classification_report =               precision    recall  f1-score   support

           0     0.6733    0.6022    0.6358     31888
           1     0.7922    0.8696    0.8291     78297
           2     0.6901    0.5894    0.6358     29303

    accuracy                         0.7496    139488
   macro avg     0.7186    0.6871    0.7002    139488
weighted avg     0.7436    0.7496    0.7443    139488

-------------------------------
Prediction horizon = 3
accuracy_score = 0.758631566873136
classification_report =               precision    recall  f1-score   support

           0     0.7005    0.7126    0.7065     38408
           1     0.8256    0.8137    0.8196     65996
           2     0.6996    0.7054    0.7025     35084

    accuracy                         0.7586    139488
   macro avg     0.7419    0.7439    0.7429    139488
weighted avg     0.7595    0.7586    0.7590    139488

-------------------------------
Prediction horizon = 4
accuracy_score = 0.7667326221610461
classification_report =               precision    recall  f1-score   support

           0     0.7009    0.8287    0.7595     47915
           1     0.8676    0.7288    0.7921     48050
           2     0.7587    0.7404    0.7494     43523

    accuracy                         0.7667    139488
   macro avg     0.7757    0.7660    0.7670    139488
weighted avg     0.7764    0.7667    0.7676    139488