from tensorflow import keras
from keras import backend as K
from transformer_layers import LayerNormalization, PositionalEncodingLayer, TransformerBlock


def get_model_attention(T, latent_dim, convolution_activation, leaky_relu_alpha, num_feature_maps):

    input_train = keras.layers.Input(shape=(T, 40, 1))
    if convolution_activation == "leaky_relu":
        conv_activation = keras.layers.LeakyReLU(alpha=leaky_relu_alpha)
    elif convolution_activation == "relu":
        conv_activation = keras.layers.ReLU()
    elif convolution_activation == "gelu":
        conv_activation = keras.layers.GELU()
    elif convolution_activation == "selu":
        conv_activation = keras.layers.selu()

    
    conv_first1 = keras.layers.Conv2D(num_feature_maps[0], (1, 2), strides=(1, 2))(input_train)
    conv_first1 = conv_activation(conv_first1)
    conv_first1 = keras.layers.Conv2D(num_feature_maps[0], (4, 1), padding='same')(conv_first1)
    conv_first1 = conv_activation(conv_first1)
    conv_first1 = keras.layers.Conv2D(num_feature_maps[0], (4, 1), padding='same')(conv_first1)
    conv_first1 = conv_activation(conv_first1)

    conv_first1 = keras.layers.Conv2D(num_feature_maps[1], (1, 2), strides=(1, 2))(conv_first1)
    conv_first1 = conv_activation(conv_first1)
    conv_first1 = keras.layers.Conv2D(num_feature_maps[1], (4, 1), padding='same')(conv_first1)
    conv_first1 = conv_activation(conv_first1)
    conv_first1 = keras.layers.Conv2D(num_feature_maps[1], (4, 1), padding='same')(conv_first1)
    conv_first1 = conv_activation(conv_first1)

    conv_first1 = keras.layers.Conv2D(num_feature_maps[2], (1, 10))(conv_first1)
    conv_first1 = conv_activation(conv_first1)
    conv_first1 = keras.layers.Conv2D(num_feature_maps[2], (4, 1), padding='same')(conv_first1)
    conv_first1 = conv_activation(conv_first1)
    conv_first1 = keras.layers.Conv2D(num_feature_maps[2], (4, 1), padding='same')(conv_first1)
    conv_first1 = conv_activation(conv_first1)

    # build the inception module
    convsecond_1 = keras.layers.Conv2D(num_feature_maps[3], (1, 1), padding='same')(conv_first1)
    convsecond_1 = conv_activation(convsecond_1)
    convsecond_1 = keras.layers.Conv2D(num_feature_maps[3], (3, 1), padding='same')(convsecond_1)
    convsecond_1 = conv_activation(convsecond_1)

    convsecond_2 = keras.layers.Conv2D(num_feature_maps[3], (1, 1), padding='same')(conv_first1)
    convsecond_2 = conv_activation(convsecond_2)
    convsecond_2 = keras.layers.Conv2D(num_feature_maps[3], (5, 1), padding='same')(convsecond_2)
    convsecond_2 = conv_activation(convsecond_2)

    convsecond_3 = keras.layers.MaxPooling2D((3, 1), strides=(1, 1), padding='same')(conv_first1)
    convsecond_3 = keras.layers.Conv2D(num_feature_maps[3], (1, 1), padding='same')(convsecond_3)
    convsecond_3 = conv_activation(convsecond_3)

    convsecond_output = keras.layers.concatenate([convsecond_1, convsecond_2, convsecond_3], axis=3)
    conv_reshape = keras.layers.Reshape((int(convsecond_output.shape[1]), int(convsecond_output.shape[3])))(convsecond_output)

    # seq2seq
    encoder_inputs = conv_reshape
    encoder = keras.layers.LSTM(latent_dim, return_state=True, return_sequences=True)
    encoder_outputs, state_h, state_c = encoder(encoder_inputs)
    states = [state_h, state_c]

    # Set up the decoder, which will only process one timestep at a time.
    decoder_inputs = keras.layers.Input(shape=(1, 3))
    decoder_lstm = keras.layers.LSTM(latent_dim, return_sequences=True, return_state=True)
    decoder_dense = keras.layers.Dense(3, activation='softmax', name='output_layer')

    all_outputs = []
    all_attention = []

    encoder_state_h = keras.layers.Reshape((1, int(state_h.shape[1])))(state_h)
    inputs = keras.layers.concatenate([decoder_inputs, encoder_state_h], axis=2)

    for _ in range(5):
        # h'_t = f(h'_{t-1}, y_{t-1}, c)
        outputs, state_h, state_c = decoder_lstm(inputs, initial_state=states)
        # dot
        attention = keras.layers.dot([outputs, encoder_outputs], axes=2)
        attention = keras.layers.Activation('softmax')(attention)
        # context vector
        context = keras.layers.dot([attention, encoder_outputs], axes=[2, 1])
        context = keras.layers.BatchNormalization(momentum=0.6)(context)

        # y = g(h'_t, c_t)
        decoder_combined_context = keras.layers.concatenate([context, outputs])
        outputs = decoder_dense(decoder_combined_context)
        all_outputs.append(outputs)
        all_attention.append(attention)

        inputs = keras.layers.concatenate([outputs, context], axis=2)
        states = [state_h, state_c]

    # Concatenate all predictions
    decoder_outputs = keras.layers.Lambda(lambda x: K.concatenate(x, axis=1), name='outputs')(all_outputs)
    decoder_attention = keras.layers.Lambda(lambda x: K.concatenate(x, axis=1), name='attentions')(all_attention)

    # Define and compile model as previously
    model = keras.models.Model([input_train, decoder_inputs], decoder_outputs)
    return model



def get_model_transformer(T, num_convolutions, num_attention_heads, num_feature_maps, num_transformers, mlp_flag):
    input = keras.layers.Input(shape=(T, 40))

    # Dilated 1-D convolution module
    dilated_conv = keras.layers.Conv1D(num_feature_maps,kernel_size=2,strides=1,activation='relu',padding='causal')(input)   
    for conv_num in range(num_convolutions-1):
        dilated_conv = keras.layers.Conv1D(num_feature_maps,kernel_size=2,dilation_rate=2**(conv_num+1),activation='relu',padding='causal')(dilated_conv)
    
    # Layer Normalization & Positional Encoding
    normalized_input = LayerNormalization()(dilated_conv)
    encoded_input = PositionalEncodingLayer()(normalized_input)

    # Transformer
    transformer_output = encoded_input
    transformer_block = TransformerBlock('Transformer_Block_1', num_attention_heads, use_masking=True)
    for _ in range(num_transformers):
        transformer_output = transformer_block(transformer_output)
    transformer_output = keras.layers.Flatten()(transformer_output)

    # MLP = Multi Layer Perceptron
    mlp_output = transformer_output
    if mlp_flag:
        mlp_output = keras.layers.Dense(units=64, activation="relu", kernel_regularizer='l2')(mlp_output)
        mlp_output = keras.layers.Dropout(rate=0.1, seed=1)(mlp_output)
    
    # Final Reshaping
    output = keras.layers.Dense(units=15)(mlp_output)
    output = keras.layers.Reshape(target_shape=(5,3))(output)
    output = keras.layers.Softmax(axis=2)(output)

    # Define and return the model
    model = keras.models.Model(input, output)
    return model



def get_model_cnn_transformer(T, num_cnn_feature_maps, num_convolutions, num_attention_heads, num_feature_maps, num_transformers, mlp_flag):
    
    input = keras.layers.Input(shape=(T, 40, 1))

    # build the CNN for feature extraction
    conv_first = keras.layers.Conv2D(num_cnn_feature_maps[0], (1, 2), strides=(1, 2))(input)
    conv_first = keras.layers.LeakyReLU(alpha=0.01)(conv_first)
    conv_first = keras.layers.Conv2D(num_cnn_feature_maps[0], (4, 1), padding='same')(conv_first)
    conv_first = keras.layers.LeakyReLU(alpha=0.01)(conv_first)
    conv_first = keras.layers.Conv2D(num_cnn_feature_maps[0], (4, 1), padding='same')(conv_first)
    conv_first = keras.layers.LeakyReLU(alpha=0.01)(conv_first)

    conv_first = keras.layers.Conv2D(num_cnn_feature_maps[1], (1, 2), strides=(1, 2))(conv_first)
    conv_first = keras.layers.LeakyReLU(alpha=0.01)(conv_first)
    conv_first = keras.layers.Conv2D(num_cnn_feature_maps[1], (4, 1), padding='same')(conv_first)
    conv_first = keras.layers.LeakyReLU(alpha=0.01)(conv_first)
    conv_first = keras.layers.Conv2D(num_cnn_feature_maps[1], (4, 1), padding='same')(conv_first)
    conv_first = keras.layers.LeakyReLU(alpha=0.01)(conv_first)

    conv_first = keras.layers.Conv2D(num_cnn_feature_maps[2], (1, 10))(conv_first)
    conv_first = keras.layers.LeakyReLU(alpha=0.01)(conv_first)
    conv_first = keras.layers.Conv2D(num_cnn_feature_maps[2], (4, 1), padding='same')(conv_first)
    conv_first = keras.layers.LeakyReLU(alpha=0.01)(conv_first)
    conv_first = keras.layers.Conv2D(num_cnn_feature_maps[2], (4, 1), padding='same')(conv_first)
    conv_first = keras.layers.LeakyReLU(alpha=0.01)(conv_first)

    # Build the inception module
    branch_a = keras.layers.Conv2D(num_cnn_feature_maps[3], (1, 1), padding='same')(conv_first)
    branch_a = keras.layers.LeakyReLU(alpha=0.01)(branch_a)
    branch_a = keras.layers.Conv2D(num_cnn_feature_maps[3], (3, 1), padding='same')(branch_a)
    branch_a = keras.layers.LeakyReLU(alpha=0.01)(branch_a)

    branch_b = keras.layers.Conv2D(num_cnn_feature_maps[3], (1, 1), padding='same')(conv_first)
    branch_b = keras.layers.LeakyReLU(alpha=0.01)(branch_b)
    branch_b = keras.layers.Conv2D(num_cnn_feature_maps[3], (5, 1), padding='same')(branch_b)
    branch_b = keras.layers.LeakyReLU(alpha=0.01)(branch_b)

    branch_c = keras.layers.MaxPooling2D((3, 1), strides=(1, 1), padding='same')(conv_first)
    branch_c = keras.layers.Conv2D(num_cnn_feature_maps[3], (1, 1), padding='same')(branch_c)
    branch_c = keras.layers.LeakyReLU(alpha=0.01)(branch_c)

    inception_output = keras.layers.concatenate([branch_a, branch_b, branch_c], axis=3)
    target_shape = (inception_output.get_shape()[1], inception_output.get_shape()[-1])
    inception_output = keras.layers.Reshape(target_shape=target_shape)(inception_output)

    # Dilated 1-D convolution module
    dilated_conv = keras.layers.Conv1D(num_feature_maps,kernel_size=2,strides=1,activation='relu',padding='causal')(inception_output)   
    for conv_num in range(num_convolutions-1):
        dilated_conv = keras.layers.Conv1D(num_feature_maps,kernel_size=2,dilation_rate=2**(conv_num+1),activation='relu',padding='causal')(dilated_conv)
    
    # Layer Normalization & Positional Encoding
    normalized_input = LayerNormalization()(dilated_conv)
    encoded_input = PositionalEncodingLayer()(normalized_input)

    # Transformer
    transformer_output = encoded_input
    transformer_block = TransformerBlock('Transformer_Block_1', num_attention_heads, use_masking=True)
    for _ in range(num_transformers):
        transformer_output = transformer_block(transformer_output)
    transformer_output = keras.layers.Flatten()(transformer_output)

    # MLP = Multi Layer Perceptron
    mlp_output = transformer_output
    if mlp_flag:
        mlp_output = keras.layers.Dense(units=64, activation="relu", kernel_regularizer='l2')(mlp_output)
        mlp_output = keras.layers.Dropout(rate=0.1, seed=1)(mlp_output)
    
    # Final Reshaping
    output = keras.layers.Dense(units=15)(mlp_output)
    output = keras.layers.Reshape(target_shape=(5,3))(output)
    output = keras.layers.Softmax(axis=2)(output)

    # Define and return the model
    model = keras.models.Model(input, output)
    return model