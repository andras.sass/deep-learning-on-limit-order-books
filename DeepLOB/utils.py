import numpy as np
from tensorflow import keras
import tensorflow as tf
from sklearn.metrics import accuracy_score, classification_report
import logging


def prepare_x(data):
    df1 = data[:40, :].T
    return np.array(df1)


def get_label(data):
    lob = data[-5:, :].T
    all_label = []

    for i in range(lob.shape[1]):
        one_label = lob[:, i] - 1
        one_label = keras.utils.to_categorical(one_label, 3)
        one_label = one_label.reshape(len(one_label), 1, 3)
        all_label.append(one_label)

    return np.hstack(all_label)


def load_train_test_data(data_preprocessing):
    if data_preprocessing == "Decimal Precision":
        abbreviation = "_DecPre_"
    elif data_preprocessing == "Z-Score":
        abbreviation = "_ZScore_"

    dec_train = np.loadtxt("data/Train_Dst_NoAuction" + abbreviation + "CF_7.txt")
    dec_test1 = np.loadtxt("data/Test_Dst_NoAuction" + abbreviation + "CF_7.txt")
    dec_test2 = np.loadtxt("data/Test_Dst_NoAuction" + abbreviation + "CF_8.txt")
    dec_test3 = np.loadtxt("data/Test_Dst_NoAuction" + abbreviation + "CF_9.txt")
    
    dec_test = np.hstack((dec_test1, dec_test2, dec_test3))

    # Extract limit order book data from the FI-2010 dataset
    train_lob = prepare_x(dec_train)
    test_lob = prepare_x(dec_test)

    # Extract label from the FI-2010 dataset
    train_label = get_label(dec_train)
    test_label = get_label(dec_test)
    return train_lob, train_label, test_lob, test_label


def data_classification(X, Y, T):
    [N, D] = X.shape
    df = np.array(X)

    dY = np.array(Y)

    dataY = dY[T - 1:N]

    dataX = np.zeros((N - T + 1, T, D))
    for i in range(T, N + 1):
        dataX[i - T] = df[i - T:i, :]

    return dataX.reshape(dataX.shape + (1,)), dataY


def prepare_decoder_input(data, teacher_forcing):
    if teacher_forcing:
        first_decoder_input = keras.utils.to_categorical(np.zeros(len(data)), 3)
        first_decoder_input = first_decoder_input.reshape(len(first_decoder_input), 1, 3)
        decoder_input_data = np.hstack((data[:, :-1, :], first_decoder_input))

    if not teacher_forcing:
        decoder_input_data = np.zeros((len(data), 1, 3))
        decoder_input_data[:, 0, 0] = 1.

    return decoder_input_data


def prepare_attention_model_data(data, labels, T, teacher_forcing=False):
    [N, D] = data.shape
    df = np.array(data)
    dY = np.array(labels)
    target = dY[T - 1:N]
    dataX = np.zeros((N - T + 1, T, D))
    for i in range(T, N + 1):
        dataX[i - T] = df[i - T:i, :]

    encoder_input = dataX.reshape(dataX.shape + (1,))

    if teacher_forcing:
        first_decoder_input = keras.utils.to_categorical(np.zeros(len(encoder_input)), 3)
        first_decoder_input = first_decoder_input.reshape(len(first_decoder_input), 1, 3)
        decoder_input = np.hstack((encoder_input[:, :-1, :], first_decoder_input))

    if not teacher_forcing:
        decoder_input = np.zeros((len(encoder_input), 1, 3))
        decoder_input[:, 0, 0] = 1.
    
    return encoder_input, decoder_input, target


def prepare_model_data(data, labels, T):
    [N, D] = data.shape
    df = np.array(data)
    dY = np.array(labels)
    target = dY[T - 1:N]
    dataX = np.zeros((N - T + 1, T, D))
    for i in range(T, N + 1):
        dataX[i - T] = df[i - T:i, :]

    input_data = dataX.reshape(dataX.shape + (1,))
    return input_data, target


def evaluation_metrics(real_y, pred_y):
    real_y = real_y[:len(pred_y)]
    logging.info('-------------------------------')

    for i in range(real_y.shape[1]):
        print(f'Prediction horizon = {i}')
        print(f'accuracy_score = {accuracy_score(np.argmax(real_y[:, i], axis=1), np.argmax(pred_y[:, i], axis=1))}')
        print(
            f'classification_report = {classification_report(np.argmax(real_y[:, i], axis=1), np.argmax(pred_y[:, i], axis=1), digits=4)}')
        print('-------------------------------')
