# Deep Learning on Limit Order Books

Application of novel deep learning frameworks to predict the direction of returns from high-frequency limit order book data

## Description
The aim of this project is to predict the direction of returns from high-frequency limit order book data, by utilizing state of the art deep learning architectures. The architectures used in this project are

    1. attention         as in https://arxiv.org/abs/2105.10430
    2. transformer       as in https://arxiv.org/pdf/1807.03819
    3. cnn-transformer   my own creation, which combines both other architectures

During this project the two former mentioned architectures were recreated, tuned and applied to the FI-2010 dataset (See PDF for more information). Additionally the third architecture cnn-transformer was created, implemented, tuned and tested.

From the abundance of incrementally trained models five are highlighted in this repository:

    1. recreated attention
    2. optimized attention
    3. recreated transformer
    4. optimized transformer
    5. optimized cnn-transformer

The models were trained to predict price movements over five time horizons k=10,20,30,50,100. The target error metric was accuracy for the prediction horizon k=100 and the aim was to outperform the attention model from https://arxiv.org/abs/2105.10430 with an accuracy of 81.45%. Unfortunately this goal was not achieved, but similarly high accuracies were obtained. The highest accuracy was achieved using the optimized attention model: 79.73%

On top of the trained deep learning model a simple trading strategy is derived to trade on unseen data. The user can choose a day and an asset to see how the trading strategy would have performed:

https://share.streamlit.io/andras-s/deeplob-trading_app/main/serving/main.py


## Time Breakdown 
- 2h    Data collection, cleaning, and transformation
- 16h   Rebuilding of established networks
- 12h   Training and fine tuning of established networks
- 8h    Selecting and building adequate state-of-the-art networks
- 14h   Training and fine tuning of new networks
- 52h   Total
- (30h  Estimated)

## Installation
The repository was developed in Google Colab and the notebooks are ment to be run in Colab on a GPU. In order to use install the repository the user should clone it to his/her own Google Drive.

## Usage
Once the repository is cloned to Google Drive, the notebooks can be opened in Colab. At the beginning of each notebook a path should specified to represent the repositories positionin the users Google Drive file structure. After that the notebooks are ready to be run. Keep in mind, that it is strongly advised to use GPU for the notebooks.

## Project status

- [x] 1. task completed 10/10
- [x] 2. task completed 10/10
- [x] 3. task completed (not evaluated yet)
